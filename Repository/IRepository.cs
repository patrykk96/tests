﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IRepository<T> where T : Entity
    {
        Task<T> GetEntity(Expression<Func<T, bool>> func);
    }
}
