﻿using Data;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AuthService : IAuthService
    {
        private readonly IRepository<User> _repo;

        public AuthService(IRepository<User> repo)
        {
            _repo = repo;
        }

        public async Task<ResultDto<BaseDto>> Login(LoginModel loginModel)
        {
            var result = new ResultDto<BaseDto>()
            {  
                Error = null 
            }; 

            var user = await _repo.GetEntity(x => x.Email == loginModel.Email);

            if (user == null)
            {
                result.Error = "Nieprawidłowy email";
                return result;
            }

            if (loginModel.Password != user.Password)
            {
                result.Error = "Nieprawidłowy hasło";
                return result;
            }

            return result;
        }
    }
}
