﻿using Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IAuthService
    {
        Task<ResultDto<BaseDto>> Login(LoginModel loginModel);
    }
}
