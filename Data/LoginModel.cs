﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
