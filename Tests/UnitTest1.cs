using Data;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Repository;
using Services;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using testy.Controllers;
using Xunit;

namespace Tests
{
    public class UnitTest1
    {
        [Fact]
        public async void ShouldReturnUnathorizedWhenEmailNotFound()
        {
            var loginModel = new LoginModel()
            {
                Email = "test@ania.pl",
                Password = "Igor"
            };

            User user = null;

            var repo = new Mock<IRepository<User>>();
            repo.Setup(x => x.GetEntity(It.IsAny<Expression<Func<User, bool>>>())).Returns(Task.FromResult(user));

            var authService = new AuthService(repo.Object);
            var authController = new AuthController(authService);

            var result = await authController.Login(loginModel);

            var unathorizedRequest = Assert.IsType<UnauthorizedObjectResult>(result);
        }

        [Fact]
        public async void ShouldReturnUnathorizedWhenPasswordIsIncorrect()
        {
            var loginModel = new LoginModel()
            {
                Email = "test@ania.pl",
                Password = "Igor"
            };

            User user = new User() 
            { 
                Email = "test@ania.pl",
                Password = "1111"
            };

            var repo = new Mock<IRepository<User>>();
            repo.Setup(x => x.GetEntity(It.IsAny<Expression<Func<User, bool>>>())).Returns(Task.FromResult(user));

            var authService = new AuthService(repo.Object);
            var authController = new AuthController(authService);

            var result = await authController.Login(loginModel);

            var unathorizedRequest = Assert.IsType<UnauthorizedResult>(result);
        }

        [Fact]
        public async void ShouldReturnOkWhenLoginDataIsCorrect()
        {
            var loginModel = new LoginModel()
            {
                Email = "test@ania.pl",
                Password = "Igor"
            };

            User user = new User()
            {
                Email = "test@ania.pl",
                Password = "Igor"
            };

            var repo = new Mock<IRepository<User>>();
            repo.Setup(x => x.GetEntity(It.IsAny<Expression<Func<User, bool>>>())).Returns(Task.FromResult(user));

            var authService = new AuthService(repo.Object);
            var authController = new AuthController(authService);

            var result = await authController.Login(loginModel);

            var unathorizedRequest = Assert.IsType<OkResult>(result);
        }
    }
}
